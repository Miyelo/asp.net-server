﻿var gulp = require('gulp'),
    jade = require('gulp-jade');


gulp.task('bioreactores', function () {
    console.log("Actualizando Bioreactores");
    return gulp.src('Bioreactores.Jade/Views**/*.jade')
        .pipe(jade({pretty:true}))// pip to jade plugin
        .pipe(gulp.dest('Bioreactores/')); // tell gulp our output folder
});

gulp.task('watchBioreactores', function () {
    gulp.watch('Bioreactores.Jade/**/*.jade', ['bioreactores']);
});
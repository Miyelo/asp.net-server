﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrossWordIA
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            InitializeBoard();
            CrossWord();
        }

        private void InitializeBoard()
        {
            board.BackgroundColor = Color.Black;
            board.DefaultCellStyle.BackColor = Color.Black;

            for (int i = 0; i < 18; i++)
                board.Rows.Add();

            foreach (DataGridViewColumn c in board.Columns)
            {
                c.Width = board.Width / board.Columns.Count;
            }

            foreach (DataGridViewRow r in board.Rows)
            {
                r.Height = board.Height / board.Rows.Count;
            }

            for (int row = 0; row < board.Rows.Count; row++)
            {
                for (int col = 0; col < board.Columns.Count; col++)
                {
                    board[col, row].ReadOnly = true;
                }
            }
        }

        private void formatCell(int row, int col, string letter)
        {
            DataGridViewCell c = board[col, row];
            c.Style.BackColor = Color.White;
            c.ReadOnly = false;
            c.Style.SelectionBackColor = Color.Cyan;
            c.Tag = letter;
        }
        private void board_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                board[e.ColumnIndex, e.RowIndex].Value = board[e.ColumnIndex, e.RowIndex].Value.ToString().ToUpper();
            }
            catch { }

            try
            {
                if (board[e.ColumnIndex, e.RowIndex].Value.ToString().Length > 1)
                {
                    board[e.ColumnIndex, e.RowIndex].Value = board[e.ColumnIndex, e.RowIndex].Value.ToString().Substring(0, 1);
                }
            }
            catch { }

            try
            {
                if (board[e.ColumnIndex, e.RowIndex].Value.ToString().Equals(board[e.ColumnIndex,e.RowIndex].Tag.ToString()))
                {
                    board[e.ColumnIndex, e.RowIndex].Style.ForeColor = Color.DarkGreen;
                }
                else
                {
                    board[e.ColumnIndex, e.RowIndex].Style.ForeColor = Color.Red;
                }
            }
            catch { }
        }

        private void board_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {

            Console.Write("(" + e.CellBounds.X + ", " + e.CellBounds.Y + ")");
            String number = "";
            bool flag = true;
            if((e.CellBounds.X == 1) && (e.CellBounds.Y == 1))
            {
                number = "1";
            }
            else if ((e.CellBounds.X == 314) && (e.CellBounds.Y == 1))
            {
                number = "5";
            }
            else if ((e.CellBounds.X == 1) && (e.CellBounds.Y == 89))
            {
                number = "2";
            }
            else if ((e.CellBounds.X == 1) && (e.CellBounds.Y == 176))
            {
                number = "4";
            }
            else if ((e.CellBounds.X == 54) && (e.CellBounds.Y == 176))
            {
                number = "2";
            }
            else if ((e.CellBounds.X == 236) && (e.CellBounds.Y == 176))
            {
                number = "4";
            }
            else if ((e.CellBounds.X == 548) && (e.CellBounds.Y == 176))
            {
                number = "7";
            }
            else if ((e.CellBounds.X == 106) && (e.CellBounds.Y == 31))
            {
                number = "1";
            }
            else if ((e.CellBounds.X == 132) && (e.CellBounds.Y == 89))
            {
                number = "3";
            }
            else if ((e.CellBounds.X == 444) && (e.CellBounds.Y == 89))
            {
                number = "6";
            }
            else if ((e.CellBounds.X == 626) && (e.CellBounds.Y == 118))
            {
                number = "8";
            }
            else if ((e.CellBounds.X == 366) && (e.CellBounds.Y == 118))
            {
                number = "3";
            }
            else if ((e.CellBounds.X == 184) && (e.CellBounds.Y == 234))
            {
                number = "5";
            }
            else if ((e.CellBounds.X == 158) && (e.CellBounds.Y == 379))
            {
                number = "6";
            }
            else if ((e.CellBounds.X == 340) && (e.CellBounds.Y == 495))
            {
                number = "7";
            }
            else
            {
                flag = false;
            }
            if(flag)
            {
                Rectangle r = new Rectangle(e.CellBounds.X, e.CellBounds.Y, e.CellBounds.Width, e.CellBounds.Height);
                e.Graphics.FillRectangle(Brushes.White, r);
                Font f = new Font(e.CellStyle.Font.FontFamily, 7);
                e.Graphics.DrawString(number, f, Brushes.Black, r);
                e.PaintContent(e.ClipBounds);
                e.Handled = true;
            }
        }

        private void CrossWord()
        {
            formatCell(0, 0, "A");
            formatCell(1, 0, "P");
            formatCell(2, 0, "R");
            formatCell(3, 0, "E");
            formatCell(4, 0, "N");
            formatCell(5, 0, "D");
            formatCell(6, 0, "I");
            formatCell(7, 0, "Z");
            formatCell(8, 0, "A");
            formatCell(9, 0, "J");
            formatCell(10, 0, "E");
            formatCell(3, 1, "C");
            formatCell(3, 2, "O");
            formatCell(3, 3, "N");
            formatCell(3, 4, "O");
            formatCell(3, 5, "M");
            formatCell(3, 6, "I");
            formatCell(3, 7, "A");
            formatCell(4, 5, "Y");
            formatCell(5, 5, "C");
            formatCell(6, 5, "I");
            formatCell(7, 5, "N");
            formatCell(6, 1, "N");
            formatCell(6, 2, "T");
            formatCell(6, 3, "E");
            formatCell(6, 4, "L");
            formatCell(6, 5, "I");
            formatCell(6, 6, "G");
            formatCell(6, 7, "E");
            formatCell(6, 8, "N");
            formatCell(6, 9, "C");
            formatCell(6, 10, "I");
            formatCell(6, 11, "A");
            formatCell(6, 12, "A");
            formatCell(6, 13, "R");
            formatCell(6, 14, "T");
            formatCell(6, 15, "I");
            formatCell(6, 16, "F");
            formatCell(6, 17, "I");
            formatCell(6, 18, "C");
            formatCell(6, 19, "I");
            formatCell(6, 20, "A");
            formatCell(6, 21, "L");
            formatCell(7, 2, "U");
            formatCell(8, 2, "R");
            formatCell(9, 2, "I");
            formatCell(10, 2, "N");
            formatCell(11, 2, "G");
            formatCell(7, 9, "H");
            formatCell(8, 9, "A");
            formatCell(9, 9, "T");
            formatCell(10, 9, "B");
            formatCell(11, 9, "O");
            formatCell(12, 9, "T");
            formatCell(13, 9, "T");
            formatCell(8, 7, "D");
            formatCell(8, 8, "R");
            formatCell(8, 10, "W");
            formatCell(8, 11, "D");
            formatCell(8, 12, "E");
            formatCell(13, 6, "T");
            formatCell(13, 7, "E");
            formatCell(13, 8, "S");
            formatCell(13, 10, "D");
            formatCell(13, 11, "E");
            formatCell(13, 12, "T");
            formatCell(13, 13, "U");
            formatCell(13, 14, "R");
            formatCell(13, 15, "I");
            formatCell(13, 16, "N");
            formatCell(13, 17, "G");
            formatCell(0, 12, "S");
            formatCell(1, 12, "I");
            formatCell(2, 12, "S");
            formatCell(3, 12, "T");
            formatCell(4, 12, "E");
            formatCell(5, 12, "M");
            formatCell(7, 12, "S");
            formatCell(9, 12, "X");
            formatCell(10, 12, "P");
            formatCell(11, 12, "E");
            formatCell(12, 12, "R");
            formatCell(14, 12, "O");
            formatCell(15, 12, "S");
            formatCell(1, 4, "C");
            formatCell(1, 5, "I");
            formatCell(1, 6, "B");
            formatCell(1, 7, "E");
            formatCell(1, 8, "R");
            formatCell(1, 9, "N");
            formatCell(1, 10, "E");
            formatCell(1, 11, "T");
            formatCell(1, 13, "C");
            formatCell(1, 14, "A");
            formatCell(3, 17, "O");
            formatCell(4, 17, "P");
            formatCell(5, 17, "T");
            formatCell(7, 17, "M");
            formatCell(8, 17, "I");
            formatCell(9, 17, "Z");
            formatCell(10, 17, "A");
            formatCell(11, 17, "R");
            formatCell(4, 14, "C");
            formatCell(4, 15, "O");
            formatCell(4, 16, "M");
            formatCell(4, 18, "U");
            formatCell(4, 19, "T");
            formatCell(4, 20, "A");
            formatCell(4, 21, "C");
            formatCell(4, 22, "I");
            formatCell(4, 23, "O");
            formatCell(4, 24, "N");
            formatCell(5, 24, "E");
            formatCell(6, 24, "U");
            formatCell(7, 24, "R");
            formatCell(8, 24, "O");
            formatCell(9, 24, "C");
            formatCell(10, 24, "I");
            formatCell(11, 24, "E");
            formatCell(12, 24, "N");
            formatCell(13, 24, "C");
            formatCell(14, 24, "I");
            formatCell(15, 24, "A");
            formatCell(7, 21, "O");
            formatCell(8, 21, "G");
            formatCell(9, 21, "I");
            formatCell(10, 21, "C");
            formatCell(11, 21, "A");
            formatCell(12, 21, "D");
            formatCell(13, 21, "I");
            formatCell(14, 21, "F");
            formatCell(15, 21, "U");
            formatCell(16, 21, "S");
            formatCell(17, 21, "A");
            formatCell(17, 13, "L");
            formatCell(17, 14, "O");
            formatCell(17, 15, "G");
            formatCell(17, 16, "I");
            formatCell(17, 17, "S");
            formatCell(17, 18, "T");
            formatCell(17, 19, "I");
            formatCell(17, 20, "C");
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProblemaDeLaMochila
{
    class Program
    {

        static void Main(string[] args)
        {
            Random generadorNumerosRandom = new Random();
            List<Objeto> ListaObjetosDisponibles = new List<Objeto>();
            Mochila mochila = new Mochila();

            Console.WriteLine("Iniciado Objetos...");

            for(int i=0;i<new Random().Next(15);i++)
            {
                double _peso = double.Parse((generadorNumerosRandom.NextDouble().ToString("0.00"))) + generadorNumerosRandom.Next(10000);
                double _costo = double.Parse((generadorNumerosRandom.NextDouble().ToString("0.00"))) + generadorNumerosRandom.Next(10000);

                ListaObjetosDisponibles.Add(new Objeto(_peso, _costo));
            }

            Console.WriteLine("Fin de Lista...");

            Console.WriteLine("Imprimiendo Lista...");

            foreach(Objeto objeto in ListaObjetosDisponibles)
            {
                Console.WriteLine(objeto.Peso + " kg , $ " + objeto.Costo);
            }

            Console.WriteLine("Fin Lista...");

            Console.WriteLine("Iniciando algoritmo boraz...");

            Objeto _objetoMasCaro=null,
                _objetoMenosPesado=null;


            //Encontramos el mas grande
            Console.WriteLine("Iniciando encontrando el más Caro y el Menos Pesado...");

            foreach (Objeto objeto in ListaObjetosDisponibles)
            {
                if(_objetoMasCaro==null)
                {
                    _objetoMasCaro = objeto;
                    _objetoMenosPesado = objeto;
                    continue;
                }

                else
                {
                    if(_objetoMasCaro.Costo<objeto.Costo)
                    {
                        _objetoMasCaro = objeto;
                    }
                    if(_objetoMenosPesado.Peso>objeto.Peso)
                    {
                        _objetoMenosPesado = objeto;
                    }
                }
            }

            Console.WriteLine("Fin encontrando el más Caro y el Menos Pesado...");

            Console.WriteLine("Iniciando optimizacion por Más Caro...");


            mochila.agregarPorPeso(_objetoMenosPesado);
            
            while(mochila.hayElementoNoElegido())
            {

            }

            Console.WriteLine("Terminando optimizacion por Más Caro...");


            Console.WriteLine("Terminado algoritmo boraz...");

            Console.ReadKey();
        }
    }
}

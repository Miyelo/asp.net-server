﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Mochila
{
    public double PesoMaximo { get; set; }
    public double pesoActual { get; set; }
    public double costoActual { get; set; }
    public int CantidadElementos { get; set; }
    public bool EstaLLeno { get; set; }

    public List<Objeto> listaObjetos = new List<Objeto>();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="objeto"></param>
    /// <returns></returns>
    public bool agregarPorCosto(Objeto objeto)
    {
        if ((objeto.Peso + pesoActual) <= PesoMaximo)
        {
            pesoActual += objeto.Peso;
            costoActual += objeto.Costo;
            objeto.YaElegido = true;
            listaObjetos.Add(objeto);
            return true;
        }
        else
            return false;
    }

    /// <summary>
    /// Agregar elemento
    /// </summary>
    /// <param name="objeto"></param>
    public bool agregarPorPeso(Objeto objeto)
    {
        if ((objeto.Peso + pesoActual) <= PesoMaximo)
        {
            pesoActual += objeto.Peso;
            costoActual += objeto.Costo;
            objeto.YaElegido = true;
            listaObjetos.Add(objeto);
            return true;
        }
        else
            return false;
    }

    /// <summary>
    /// Si hay un elemento no elegido
    /// </summary>
    /// <returns></returns>
    public bool hayElementoNoElegido()
    {

        foreach(Objeto objeto in listaObjetos)
        {
            if (objeto.YaElegido == false)
                return true;
        }
        return false;
    }

    /// <summary>
    /// Reiniciar los objetos elegidos de la listaObjetos
    /// </summary>
    public void reiniciarElegidosListaObjetos()
    {
        foreach(Objeto objeto in listaObjetos)
        {
            objeto.YaElegido = false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Objeto
{
    public double Peso { get; set; }
    public double Costo { get; set; }
    public bool YaElegido { get; set; }

    public Objeto(double peso, double costo)
    {
        this.Peso = peso;
        this.Costo = costo;
        this.YaElegido = false;
    }
}

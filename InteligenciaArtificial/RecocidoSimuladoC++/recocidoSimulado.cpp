#define _CRT_SECURE_NO_WARNINGS
#include <math.h>
#include <stdio.h>


#define MBIG 1000000000 /* According to Knuth, any large MBIG, and */
#define MSEED 161803398 /* any smaller (but still large MSEED can  */
#define MZ 0
#define FAC (1.0/MBIG)


/* Constantes */
enum Boolean { FALSE, TRUE };
const float pMax = 10.0;
const float pMin = -10.0;
const int longCadena = 50;
const int maxIntentos = 100;
const int minIntentos = 20;
const float minRazonAceptacion = 0.8;
const float alfa = 0.8;
const float beta = 1.2;
const int maxCadenas = 10;
const int frecImpresion = 10;

class Punto {
public:
	float x, y;
	float evaluacion;
	void Imprime(FILE*);
	void Copia(Punto*);
	void Evalua();
	void Inicializa();
	Punto GeneraVecino();
	Boolean AceptaIntento(Punto*, float);
	void CadenaMarkov(float);
	void Recocido(float);
};

/* Variables Globales */
float temp;
int   totalIntentos;
long  idum;
Punto mejor;
FILE *salida;

float ran3(long *idnum)
/********************************************************************
* Returns a uniform random deviate between 0.0 and 1.0. Set idnum   *
* to any negative value to initialize or reinitialize the sequence. *
********************************************************************/
{
	static int inext, inextp;
	static long ma[56];   /* The value 56 (range ma[1..55]) is special */
						  /* and should not be modified; see Knuth.    */
	static int iff = 0;
	long mj, mk;
	int i, ii, k;

	if (*idnum<0 || iff == 0) {    /* Initialization */
		iff = 1;
		/* Initialize ma[55] using the seed idnum and the large number MSEED */
		mj = MSEED - (*idnum<0 ? -*idnum : *idnum);
		mj %= MBIG;
		ma[55] = mj;
		mk = 1;
		/* Now initizalize the rest of the table, in a slightly       */
		/* random order, with numbers that are not especially random. */
		for (i = 1; i <= 54; i++) {
			ii = (21 * i) % 55;
			ma[ii] = mk;
			mk = mj - mk;
			if (mk < MZ) mk += MBIG;
			mj = ma[ii];
		}
		/* We randomize them by "warming up the generator." */
		for (k = 1; k <= 4; k++)
			for (i = 1; i <= 55; i++) {
				ma[i] -= ma[1 + (i + 30) % 55];
				if (ma[i]<MZ) ma[i] += MBIG;
			}
		inext = 0;     /* Prepare indices for our first generated number. */
		inextp = 31;   /* The constant 31 is special; see Knuth */
		*idnum = 1;
	}
	/* Here is where we start, except on initialization */
	if (++inext == 56) inext = 1;   /* Initizalize inext and inextp, wrapping    */
	if (++inextp == 56) inextp = 1; /* arround 56 to 1.                          */
	mj = ma[inext] - ma[inextp];  /* Generate new random number substractively */
	if (mj<MZ) mj += MBIG;         /* Make sure that it is in range.            */
	ma[inext] = mj;               /* Store it                                  */
	return mj*FAC;                /* and output the derived uniform deviate.   */
} /* ran3 */


  /* Exponencial */
float Exponencial(float x) {
	const float grande = 50.0;
	if (x < -grande)
		return 0.0;
	else
		return exp((double)x);
}  


/* Imprime un Punto u */
void Punto::Imprime(FILE* archivo) {
	
	fprintf(archivo, "f(%5.2f,%5.2f) = %8.2f", x, y, evaluacion);
}

/* Imprime */
void Punto::Copia(Punto* origen) {
	x = origen->x;
	y = origen->y;
	evaluacion = origen->evaluacion;
} 

float FcnObjetivo(Punto* punto) {
	/**********************************
	* Implementa la funcion objetivo *
	* En este ejemplo implementa la  *
	* funcion f(x,y) = x^2 + y^2     *
	**********************************/
	return (punto->x)*(punto->x) + (punto->y)*(punto->y);
}

void Punto::Evalua() {
	/******************************************
	* Evalua un Punto u guarda su evaluacion *
	******************************************/
	evaluacion = FcnObjetivo(this);
	if (evaluacion < mejor.evaluacion)
		mejor.Copia(this);
	if ((totalIntentos % frecImpresion) == 0) {
		fprintf(salida, "Intentos =%6d ", totalIntentos);
		mejor.Imprime(salida);
		fprintf(salida, "\n");
	}
	totalIntentos++;
} /* Evalua */

void Punto::Inicializa() {
	/***********************
	* Inicializa un Punto *
	***********************/
	printf("Dame punto inicial: ");
	scanf("%f%f", &x, &y);
	totalIntentos = 0;
	/* Se evalua directamente para evitar comparacion */
	/* con mejor que todavia no contiene nada         */
	evaluacion = FcnObjetivo(this);
	mejor.Copia(this);
	/* Se EvaluaPunto para permitir */
	/* impresion del primer punto   */
	Evalua();
	printf("punto inicial: ");
	Imprime(stdout);
	printf("\n");
} /* Inicializa */

Punto Punto::GeneraVecino() {
	/*******************************
	* Regresa un vecino del Punto *
	*******************************/
	const float tamanoVecindad = 0.1;
	Punto aux;

	aux.Copia(this);
	aux.x = aux.x + tamanoVecindad * (ran3(&idum) - 0.5);
	aux.y = aux.y + tamanoVecindad * (ran3(&idum) - 0.5);
	if (aux.x > pMax) aux.x = pMax;
	else
		if (aux.x < pMin) aux.x = pMin;
	if (aux.y > pMax) aux.y = pMax;
	else
		if (aux.y < pMin) aux.y = pMin;
	aux.Evalua();
	return aux;
} /* GeneraVecino */

Boolean Punto::AceptaIntento(Punto *v, float c) {
	/****************************************
	* Regresa verdadero si se debe aceptar *
	* un punto nuevo v dado un Punto       *
	****************************************/
	if (v->evaluacion <= evaluacion)
		return TRUE;
	else
		if (ran3(&idum) < Exponencial(-(v->evaluacion - evaluacion) / c))
			return TRUE;
		else
			return FALSE;
}  /* AceptaIntento */

void Punto::CadenaMarkov(float c) {
	/*******************************
	* Ejecuta una cadena de Markov *
	* a una temperatura fija c     *
	*******************************/
	Punto v;
	int intentos = 0;
	int intentosAceptados = 0;

	while ((intentosAceptados < longCadena) && (intentos < maxIntentos)) {
		v = GeneraVecino();
		intentos++;
		if (AceptaIntento(&v, c)) {
			Copia(&v);
			intentosAceptados++;
		}
	}
} /* CadenaMarkov */

void Punto::Recocido(float c) {
	/**********************************************
	* Implementa el algoritmo basico de recocido; *
	* regresa el ultimo punto visitado            *
	**********************************************/
	int cadenasSinMejora = 0;
	Punto anterior;

	anterior.Copia(this);
	printf("temp. =%4.1f ", c);
	printf("intentos =%5d", totalIntentos);
	Imprime(stdout);
	printf("\n");
	do {
		CadenaMarkov(c);
		if (evaluacion >= anterior.evaluacion)
			cadenasSinMejora++;
		else
			cadenasSinMejora = 0;
		printf("temperatura =%4.1f ", c);
		printf("intentos =%5d ", totalIntentos);
		Imprime(stdout);
		printf(" sin mejora =%3d\n", cadenasSinMejora);
		anterior.Copia(this);
		c *= alfa;
	} while (cadenasSinMejora < maxCadenas);
} /* Recocido */

float Inicializar(Punto* u)
/**********************************
* Regresa una temperatura inicial *
**********************************/
{
	Punto v;
	int intentos = 0;
	int intentosAceptados = 0;
	float  c = 1.0;

	do {
		v = u->GeneraVecino();
		v.Imprime(stdout);
		intentos++;
		if (u->AceptaIntento(&v, c) == TRUE) {
			u->Copia(&v);
			intentosAceptados++;
		}
		c *= beta;
	} while ((intentos <= minIntentos) ||
		((1.0*intentosAceptados) / intentos <= minRazonAceptacion));
	printf("temp incial =%4.1f", c);
	printf(" intentos =%5d", intentos);
	printf(" intentos aceptados =%5d\n", intentosAceptados);
	return c;
} /* Inicializar */

void InicializarRan3()
/*******************************
* Inicializa el generador ran3 *
*******************************/
{
	printf("Semilla del generador de numeros aleatorios: ");
	scanf("%ld", &idum);
	if (idum>0) idum *= -1;
} /* InicializarRan3 */

int main() {
	Punto punto;

	InicializarRan3();
	salida = fopen("salida", "w");
	printf("*** Inicio del programa ***\n");
	punto.Inicializa();
	temp = Inicializar(&punto);
	punto.Recocido(temp);
	printf("\n");
	printf("Ultimo punto encontrado: ");
	punto.Imprime(stdout);
	printf("\n");
	printf("Mejor punto encontrado:  ");
	mejor.Imprime(stdout);
	printf("\n");
	scanf("%ld", &idum);
	fclose(salida);
} /* main */



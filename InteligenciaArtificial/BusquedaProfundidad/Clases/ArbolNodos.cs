﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class ArbolNodos
{
    public Nodo NodoInicial;
    public int NumeroTotalNodos;

    public ArbolNodos()
    {
        this.NodoInicial = crearArbol();
    }

    Nodo crearArbol()
    {
        Nodo _NodoInicial = new Nodo();

        NumeroTotalNodos = new Random(DateTime.Now.Millisecond).Next(100000) + 30;
        int TotalNodosAsignados = 0;

        while (TotalNodosAsignados<=NumeroTotalNodos)
        {
            RecorrerArbol(_NodoInicial);
            TotalNodosAsignados++;
        }                        

        return _NodoInicial;
    }

    void RecorrerArbol( Nodo NodoActual)
    {
        if(NodoActual != null)
        {
            if (NodoActual.NodoIzquierdo == null)
            {
                NodoActual.NodoIzquierdo = new Nodo();
                return;
            }            

            if(NodoActual.NodoDerecho==null)
            {
                NodoActual.NodoDerecho = new Nodo();
                return;
            } 

            if((DateTime.Now.Millisecond)%2==0)
            {
                RecorrerArbol(NodoActual.NodoIzquierdo);
            }
            else
            {
                RecorrerArbol(NodoActual.NodoDerecho);
            }
        }
    }
}    



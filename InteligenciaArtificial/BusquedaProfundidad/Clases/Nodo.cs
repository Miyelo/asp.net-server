﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class Nodo
{
    public static int ContadorNumeroNodo = 0;

    public int NumeroNodo;
    public Nodo NodoIzquierdo,
        NodoDerecho;
    public bool YaRecorrido;

    public Nodo()
    {
        this.NumeroNodo =++ContadorNumeroNodo;
        this.YaRecorrido = false;
    }
}

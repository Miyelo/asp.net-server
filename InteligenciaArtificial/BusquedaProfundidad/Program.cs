﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusquedaProfundidad
{
    class Program
    {
        static List<string> RecorridoNodos = new List<string>();
        static void Main(string[] args)
        {
            if (File.Exists("recorrido.txt"))
                File.Delete("recorrido.txt");
            //Crear Arbol
            Console.WriteLine("Creando Arbol...");
            ArbolNodos Arbol = CrearArbol();
            Console.WriteLine("Termino de crear Arbol, total de nodos="+Arbol.NumeroTotalNodos);

            //Empieza la simulacion de profundidad
            DateTime TiempoInicio = DateTime.Now;
            Console.WriteLine("Comienza Recorrido...");
            RecorrerArbol(Arbol);

            //Termina la simulacion de profundidad
            Console.WriteLine("Termina Recorrido");
            DateTime TiempoFinal = DateTime.Now;
            TimeSpan TiempoTotal = TiempoFinal - TiempoInicio;
            Console.WriteLine("Tiempo total = " + TiempoTotal.ToString("hh\\:mm\\:ss\\.ffff"));
            File.WriteAllLines("recorrido.txt", RecorridoNodos.ToList());
            Console.ReadKey();
        }

        static ArbolNodos CrearArbol()
        {
            return new ArbolNodos();
        }

        static void RecorrerArbol(ArbolNodos Arbol)
        {
            RecorrerNodo(Arbol.NodoInicial);
        }

        static void RecorrerNodo(Nodo nodoActual)
        {
            if(nodoActual!=null)
            {
                if(nodoActual.NodoIzquierdo!=null)
                {
                    nodoActual.YaRecorrido = true;
                    RecorridoNodos.Add("Nodo Recorrido=>" + nodoActual.NumeroNodo);
                    RecorrerNodo(nodoActual.NodoIzquierdo);
                }

                if(nodoActual.NodoDerecho!=null)
                {
                    nodoActual.YaRecorrido = true;
                    RecorridoNodos.Add("Nodo Recorrido=>" + nodoActual.NumeroNodo);
                    RecorrerNodo(nodoActual.NodoDerecho); 
                }
            }
        }
    }
}

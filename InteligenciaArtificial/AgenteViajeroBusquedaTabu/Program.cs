﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgenteViajeroBusquedaTabu
{
    class Program
    {
        static int n = 20;
        static double[,] MatrizAdyacentes = new double[n, n];
        static Dictionary<string, string> ListaTabu = new Dictionary<string, string>();

        static void Main(string[] args)
        {


            List<Ciudad> Ciudades = new List<Ciudad>();
            int MaximoIteraciones = 500;


            for (int i = 0; i < n; i++)
                Ciudades.Add(new Ciudad());

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i != j)
                        MatrizAdyacentes[i, j] = MatrizAdyacentes[j, i] = Ciudades.ElementAt(i).DistanciaEntreCiudad(Ciudades.ElementAt(j));
                    else
                        MatrizAdyacentes[i, i] = 0;
                }
            }

            double[] SolucionInicial = new double[n + 2];
            for (int i = 0; i < n; i++)
                SolucionInicial[i] = i + 1;
            SolucionInicial[n] = 1;
            SolucionInicial[n + 1] = getDistSol(SolucionInicial);
            double[] SolucionMejor = (double [])SolucionInicial.Clone();

            Console.WriteLine(solToString(SolucionMejor));
            Console.WriteLine(SolucionMejor[n + 1]);

            int cont = 0;
            while (cont < MaximoIteraciones)
            {
                SolucionInicial = permutar(SolucionInicial);
                if (SolucionInicial == null)
                {
                    Console.WriteLine("Se acabaron los posibles vecinos");
                    break;
                }
                if (SolucionInicial[n + 1] >= SolucionMejor[n + 1])
                {

                    cont++;
                }
                else {
                    SolucionMejor =(double[]) SolucionInicial.Clone();
                    Console.WriteLine("La mejor");
                }
            }
            Console.WriteLine(solToString(SolucionMejor));
            Console.WriteLine(SolucionMejor[n + 1]);

            Console.ReadKey();
        }

        static double getDistSol(double[] sol)
        {
            double dist = 0;
            for (int i = 0; i < sol.Length - 2; i++)
            {
                dist += MatrizAdyacentes[(int)(sol[i] - 1), (int)(sol[i + 1] - 1)];
            }
            return dist;
        }

        static void imprimirMatriz(double[][] mat)
        {
            for (int i = 0; i < mat.Length; i++)
            {
                for (int j = 0; j < mat[i].Length; j++)
                {
                    Console.WriteLine(mat[i][j] + " ");
                }
                Console.WriteLine("");
            }
        }

        static String solToString(double[] sol)
        {
            String str = "";
            for (int i = 0; i < sol.Length - 2; i++)
            {
                str += (int)(sol[i]) + ",";
            }
            str += (int)sol[0];
            return str;
        }

        static double[] permutar(double[] sol)
        {
            double temp, minV = 0;
            int minI = 0;
            Boolean band = false;
            double[][] per = new double[n - 1][];

            for (int i = 0; i < per.Length; i++)
            {
                per[i] = new double[n + 2];
            }

            for (int i = 0; i < n - 1; i++)
            {
                per.SetValue(sol.Clone(), i);
                //swap
                temp = per[i][i];
                per[i][i] = per[i][i + 1];
                per[i][i + 1] = temp;
                //
                per[i][n] = per[i][0];
                per[i][n + 1] = getDistSol(per[i]);

                if (!ListaTabu.ContainsKey(solToString(per[i])))
                {
                    if (band == false)
                    {
                        minV = per[i][n + 1];
                        minI = i;
                        band = true;
                    }
                    else {
                        if (per[i][n + 1] < minV)
                        {
                            minV = per[i][n + 1];
                            minI = i;
                        }
                    }
                }
                if (band)
                {
                    Console.WriteLine("Se tomo la permutacion " + minI + " " + solToString(per[minI]) + " " + per[minI][n + 1]);
                    ListaTabu.Add(solToString(per[minI]), "1");
                    return per[minI];
                }
                else
                {
                    return null;
                }
            }
            return null;
        }
    }
}

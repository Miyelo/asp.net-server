﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Ciudad
{
    public double x { get; set; }
    public double y { get; set; }

    public Ciudad()
    {
        Random _SemillaRandom = new Random(DateTime.Now.Millisecond);
        this.x = _SemillaRandom.NextDouble()*100;
        this.y = _SemillaRandom.NextDouble() * 100;
    }

    public Ciudad(double x,double y)
    {
        this.x = x;
        this.y = y;
    }

    public double DistanciaEntreCiudad(Ciudad vecino)
    {
        return Math.Sqrt(Math.Pow(x - vecino.x, 2) + Math.Pow(y - vecino.y, 2));
    }
}

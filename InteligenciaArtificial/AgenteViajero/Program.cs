﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


class Program
{
    static void Main(string[] args)
    {
        List<Nodo> ListaNodos = new List<Nodo>();
        string[] lineasTexto = File.ReadAllLines("C:/Users/miyel/Desktop/ulysses22.tsp");
        foreach(string lineaTexto in lineasTexto)
        {
            try
            {
                string[] lineaTextoSeparada = lineaTexto.Split(' ');
                string _numeroNodo = lineaTextoSeparada[0],
                    _coordenadaX = lineaTextoSeparada[1],
                    _coordenadaY = lineaTextoSeparada[2];
                ListaNodos.Add(new Nodo(_numeroNodo, _coordenadaX, _coordenadaY));
            }
            catch (Exception) { }
        }
        Console.WriteLine("Fin de Leer Archivo");
        /*Fin de lectura de archivo*/

        Console.WriteLine("Empieza agente viajero");
        AgenteViajero agenteViajero = new AgenteViajero();

        for (int x=0;x<ListaNodos.Count;x++)
        {
            agenteViajero.viajar(ListaNodos, x);
            double cantidadViaje = 0;
            for (int i = 0; i < agenteViajero.ListaViajeRealizado.Count; i++)
            {
                try
                {
                    Nodo nodoActual = agenteViajero.ListaViajeRealizado[i];
                    Nodo nodoSiguiente = agenteViajero.ListaViajeRealizado[i + 1];
                    nodoActual.YaViajado = false;
                    Console.WriteLine("Nodo [" + nodoActual.Numero + "] a Nodo [" + nodoSiguiente.Numero + "] = " + nodoSiguiente.Distancia);
                    cantidadViaje += nodoSiguiente.Distancia;
                }
                catch (Exception)
                {
                    Nodo nodoActual = agenteViajero.ListaViajeRealizado[i];
                    nodoActual.YaViajado = false;
                    Nodo nodoSiguiente = agenteViajero.ListaViajeRealizado[0];

                    double X = (Math.Pow(nodoActual.CoordenadaX, 2)) + (Math.Pow(nodoSiguiente.CoordenadaX, 2));
                    double Y = (Math.Pow(nodoActual.CoordenadaY, 2)) + (Math.Pow(nodoSiguiente.CoordenadaY, 2));
                    double _distancia = Math.Sqrt((X + Y));

                    Console.WriteLine("Nodo [" + nodoActual.Numero + "] a Nodo [" + nodoSiguiente.Numero + "] = " + _distancia);
                    cantidadViaje += nodoSiguiente.Distancia;
                    ListaNodos[x].TotalViaje = cantidadViaje;
                    Console.WriteLine("Cantidad Total=" + cantidadViaje);
                    
                }
            }

            for (int i = 0; i < agenteViajero.ListaViajeRealizado.Count; i++)
            {
                if (agenteViajero.ListaViajeRealizado[i] == agenteViajero.ListaViajeRealizado.Last())
                    Console.Write(agenteViajero.ListaViajeRealizado[i].Numero + "-" + agenteViajero.ListaViajeRealizado[0].Numero);
                else
                    Console.Write(agenteViajero.ListaViajeRealizado[i].Numero + "-");

            }
        }
            Console.ReadKey();
    }
}

public class Nodo
{
    public double Numero, CoordenadaX, CoordenadaY;
    public bool YaViajado=false;
    public double Distancia, TotalViaje;

    public Nodo() { }

    public Nodo(string numero,string coordenadaX,string coordenadaY)
    {
        this.Numero =double.Parse( numero);
        this.CoordenadaX = double.Parse(coordenadaX);
        this.CoordenadaY = double.Parse(coordenadaY);
    }
}

public class AgenteViajero
{
    public List<Nodo> ListaViajeRealizado = new List<Nodo>();

    public void viajar(List<Nodo>listaNodos, int inicioNodo)
    {
        //int numeroDeNodoRandom = new Random().Next(listaNodos.Count);
        int numeroDeNodoRandom = inicioNodo;
        listaNodos.ElementAt(numeroDeNodoRandom).YaViajado = true;

        ListaViajeRealizado.Add(listaNodos.ElementAt(numeroDeNodoRandom));
        bool YaTermino = false;


        ///Cada nodo va a buscar de los que faltan
        ///
        while(!YaTermino)
        {

            Nodo _nodoActual = ListaViajeRealizado.Last(),
            _nodoTemporalMenor=null;

            bool HayUnoSinViajar = false;

            foreach (Nodo nodo in listaNodos)
            {
                //Si el nodo no ha sido pisado y no es el mismo
                if (!nodo.YaViajado && _nodoActual.Numero != nodo.Numero)
                {
                    HayUnoSinViajar = true;
                    double X = (Math.Pow(_nodoActual.CoordenadaX, 2)) + (Math.Pow(nodo.CoordenadaX, 2));
                    double Y = (Math.Pow(_nodoActual.CoordenadaY, 2)) + (Math.Pow(nodo.CoordenadaY, 2));
                    double _distancia = Math.Sqrt((X + Y));

                    nodo.Distancia = _distancia;

                    if (_nodoTemporalMenor == null)
                        _nodoTemporalMenor = nodo;
                    else
                        if (nodo.Distancia < _nodoTemporalMenor.Distancia)
                            _nodoTemporalMenor = nodo;                                                      
                }       
            }
            if (!HayUnoSinViajar)
                break;

            _nodoTemporalMenor.YaViajado = true;
            ListaViajeRealizado.Add(_nodoTemporalMenor);            
        }

        //Calculamos la distancia para regresar                

    }
}

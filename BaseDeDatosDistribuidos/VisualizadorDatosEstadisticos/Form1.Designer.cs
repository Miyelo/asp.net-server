﻿namespace VisualizadorDatosEstadisticos
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.analizador_estadistico = new VisualizadorDatosEstadisticos.Modelo.analizador_estadistico();
            this.analizadorestadisticoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vistaventastotalesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vista_ventas_totalesTableAdapter = new VisualizadorDatosEstadisticos.Modelo.analizador_estadisticoTableAdapters.vista_ventas_totalesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.analizador_estadistico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.analizadorestadisticoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vistaventastotalesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.DataSource = this.vistaventastotalesBindingSource;
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(12, 12);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            series1.IsValueShownAsLabel = true;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            series1.XValueMember = "NOMBRE_EMPRESA";
            series1.YValueMembers = "sum(lista_ventas_CANTIDAD_VENTA)";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(807, 444);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // analizador_estadistico
            // 
            this.analizador_estadistico.DataSetName = "analizador_estadistico";
            this.analizador_estadistico.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // analizadorestadisticoBindingSource
            // 
            this.analizadorestadisticoBindingSource.DataSource = this.analizador_estadistico;
            this.analizadorestadisticoBindingSource.Position = 0;
            // 
            // vistaventastotalesBindingSource
            // 
            this.vistaventastotalesBindingSource.DataMember = "vista_ventas_totales";
            this.vistaventastotalesBindingSource.DataSource = this.analizadorestadisticoBindingSource;
            // 
            // vista_ventas_totalesTableAdapter
            // 
            this.vista_ventas_totalesTableAdapter.ClearBeforeFill = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(821, 468);
            this.Controls.Add(this.chart1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.analizador_estadistico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.analizadorestadisticoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vistaventastotalesBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.BindingSource analizadorestadisticoBindingSource;
        private Modelo.analizador_estadistico analizador_estadistico;
        private System.Windows.Forms.BindingSource vistaventastotalesBindingSource;
        private Modelo.analizador_estadisticoTableAdapters.vista_ventas_totalesTableAdapter vista_ventas_totalesTableAdapter;
    }
}


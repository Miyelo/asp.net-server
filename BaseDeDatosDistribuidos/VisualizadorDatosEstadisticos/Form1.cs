﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisualizadorDatosEstadisticos
{
    public partial class Form1 : Form
    {
        Timer EjecutarRecargaDatos;
        public Form1()
        {
            InitializeComponent();
            this.EjecutarRecargaDatos = new Timer();
            this.EjecutarRecargaDatos.Interval = 100;
            this.EjecutarRecargaDatos.Tick += EjecutarRecargaDatos_Tick;
            this.EjecutarRecargaDatos.Start();
        }

        private void EjecutarRecargaDatos_Tick(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'analizador_estadistico.vista_ventas_totales' table. You can move, or remove it, as needed.
            this.vista_ventas_totalesTableAdapter.Fill(this.analizador_estadistico.vista_ventas_totales);
            this.chart1.DataBind();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'analizador_estadistico.vista_ventas_totales' table. You can move, or remove it, as needed.
            this.vista_ventas_totalesTableAdapter.Fill(this.analizador_estadistico.vista_ventas_totales);

        }
    }
}

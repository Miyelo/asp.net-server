﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;


public class ConexionMysql
{
    public string DireccionServidor { get; set; }
    public string NombreBaseDatos { get; set; }
    public string Usuario { get; set; }
    public string Contraseña { get; set; }
    public bool HayConexion { get; set; }
    public MySqlConnection Connection;

    public ConexionMysql(string _DireccionServidor)
    {
        this.DireccionServidor = _DireccionServidor;
        this.NombreBaseDatos = "analizador_compras";
        this.Usuario = "programas";
        this.Contraseña = "programas";
        this.Connection = new MySqlConnection("server=" + DireccionServidor
            + ";Database=" + NombreBaseDatos
            + ";uid=" + Usuario
            + ";password=" + Contraseña
            + ";");
        try
        {
            Connection.Open();
            this.HayConexion = true;
        }
        catch
        {
            this.HayConexion = false;
        }
    }    

    public DataTable Select(string _Query)
    {
        DataTable _TablaResultado = new DataTable();
        if(Connection.State==ConnectionState.Closed)
            Connection.Open();

        try
        {
            MySqlCommand _MysqlCommand = Connection.CreateCommand();
            _MysqlCommand.EnableCaching = true;
            _MysqlCommand.CommandText = _Query;
            _MysqlCommand.ExecuteNonQuery();            
            MySqlDataAdapter _MysqlAdapter = new MySqlDataAdapter(_MysqlCommand);
            _MysqlAdapter.Fill(_TablaResultado);
            Connection.Close();
            return _TablaResultado;
        }

        catch
        {
            Connection.Close();
            return null;
        }
    }

    public bool Insert(string _Query)
    {
        if(Connection.State==ConnectionState.Closed)
            Connection.Open();
        try
        {
            MySqlCommand _MysqlCommand = Connection.CreateCommand();
            _MysqlCommand.EnableCaching = true;
            _MysqlCommand.CommandText = (_Query);
            _MysqlCommand.ExecuteNonQuery();
            Connection.Close();
            return true;
        }
        catch
        {
            Connection.Close();
            return false;
        }
    }
}

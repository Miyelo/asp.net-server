﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Threading;

namespace GeneradorDatosEstadisticos
{
    class Program
    {
        static void Main(string[] args)
        {
            //Conectarse a la bd
            ConexionMysql ConexionMysql;
            DataTable MysqlTablaResultado;
            List<Empresa> ListaEmpresas = new List<Empresa>();

            int _Indice = 0;
            do
            {
                ConexionMysql = new ConexionMysql(VG.ListaServidores[_Indice]);

                if (_Indice < VG.ListaServidores.Count)
                    _Indice++;
                else
                    _Indice = 0;
            }
            while (!ConexionMysql.HayConexion);

            MysqlTablaResultado = ConexionMysql.Select
                (
                    "select * from empresa"
                );

            foreach(DataRow fila in MysqlTablaResultado.Rows)
            {
                string _ID_EMPRESA = fila["ID_EMPRESA"] + "",
                    _NOMBRE_EMPRESA = fila["NOMBRE_EMPRESA"] + "";

                ListaEmpresas.Add(new Empresa(_ID_EMPRESA, _NOMBRE_EMPRESA));
            }

            while (true)
            {
                Thread.Sleep(DateTime.Now.Millisecond);

                foreach(Empresa _Empresa in ListaEmpresas)
                {
                    if (DateTime.Now.Millisecond > 500)
                        _Empresa.InsertarProduccion();                        
                }

                //Recargar empresas
                MysqlTablaResultado = ConexionMysql.Select
               (
                   "select * from empresa"
               );

                ListaEmpresas.Clear();
                foreach (DataRow fila in MysqlTablaResultado.Rows)
                {
                    string _ID_EMPRESA = fila["ID_EMPRESA"] + "",
                        _NOMBRE_EMPRESA = fila["NOMBRE_EMPRESA"] + "";

                    ListaEmpresas.Add(new Empresa(_ID_EMPRESA, _NOMBRE_EMPRESA));
                }
            }
        }
    }    
}

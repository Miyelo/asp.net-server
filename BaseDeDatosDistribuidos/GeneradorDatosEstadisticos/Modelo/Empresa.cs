﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Empresa
{
    public string NombreEmpresa { get; set; }
    public ConexionMysql ConexionMysql { get; set; }
    public int IdEmpresa { get; set; }

    public Empresa (string IdEmpresa,string NombreEmpresa)
    {
        this.NombreEmpresa = NombreEmpresa;
        this.IdEmpresa = int.Parse(IdEmpresa);

        int _Indice = 0;
        do
        {
            this.ConexionMysql = new ConexionMysql(VG.ListaServidores[_Indice]);

            if (_Indice < VG.ListaServidores.Count)
                _Indice++;
            else
                _Indice = 0;
        }
        while (!this.ConexionMysql.HayConexion);
    }

    public void InsertarProduccion()
    {
        double cantidadIngresada = (new Random(DateTime.Now.Millisecond).NextDouble())*10000;
        this.ConexionMysql.Insert
            (
                "Insert into lista_ventas "
                + "(CANTIDAD_VENTA,ID_EMPRESA) VALUES "
                + "(" + cantidadIngresada + "," + IdEmpresa + ")"
            );
        Console.WriteLine("Inserto Empresa[" + this.NombreEmpresa + "] Cantidad=" + cantidadIngresada);
    }
}

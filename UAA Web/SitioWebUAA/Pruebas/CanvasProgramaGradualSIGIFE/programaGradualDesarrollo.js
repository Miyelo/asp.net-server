﻿var TamañoImagen_X = 413, TamañoImagen_Y = 278,
    TamañoCanvas_X = 413, TamañoCanvas_Y = 278;
var canvas;
function Rectangulo(posicionX,posicionY,cantidad)
{
    this.posicionX = posicionX;
    this.posicionY = posicionY;
    this.cantidad = '$'+cantidad;
}

/**/
window.onload=function()
{
    canvas = document.getElementById("imagenProgramaGradual").getContext("2d");

    pintarImagenFondo(canvas);

    canvas2 = document.getElementById("programaGradualEquipamientoLargoPlazo").getContext("2d");

    pintarImagenFondoEquipamiento(canvas2);
}

function pintarImagenFondoEquipamiento(canvas) {
    var imagen = new Image();
    imagen.src = "programaGradualEquipamientoLargoPlazo.png";
    imagen.onload = function () {
        canvas.drawImage(imagen, 0, 0);
        pintarValoresEquipamiento(canvas);
    }
}


function pintarImagenFondo(canvas)
{
    var imagen = new Image();
    imagen.src = "programaGradualDesarrollo.png";
    imagen.onload = function ()
    {
        canvas.drawImage(imagen, 0, 0);
        pintarValores(canvas);
    }
}

function pintarValoresEquipamiento(canvas) {
    var TamañoLetra = 12;

    /*Maximo 6 digitos en cantidad*/
    var valoresEnLosRectangulos = [
        A_verde = new Rectangulo(53, 68, 11111),
        B_verde = new Rectangulo(114, 109, 123456),
        B_rojo = new Rectangulo(114, 123, 123),
        C_verde = new Rectangulo(201, 155, 123),
        C_rojo = new Rectangulo(201, 170, 132),
        D_verde = new Rectangulo(286, 202, 123),
        D_rojo = new Rectangulo(286, 215, 123)
    ];

    canvas2.fillStyle = "white";
    canvas2.font = "" + TamañoLetra + "px Arial"


    //canvas2.textAlign="center"
    for (i = 0; i < valoresEnLosRectangulos.length; i++) {
        var rectangulo = valoresEnLosRectangulos[i];
        canvas2.fillText("" + rectangulo.cantidad, rectangulo.posicionX, rectangulo.posicionY + TamañoLetra);
    }
}
function pintarValores(canvas)
{
    var TamañoLetra = 12;

    /*Maximo 6 digitos en cantidad*/
    var valoresEnLosRectangulos=[ 
        A_verde = new Rectangulo(46, 43, 11111),
        A_rojo = new Rectangulo(46, 58, 555555),
        A_azul = new Rectangulo(46, 71, 999999),
        B_verde = new Rectangulo(99,99,123456),
        B_rojo = new Rectangulo(99,114,123),
        B_azul = new Rectangulo(99,128,123),
        C_verde = new Rectangulo(149,152,123),
        C_rojo = new Rectangulo(149,167,132),
        C_azul = new Rectangulo(149,181,123),
        D_verde = new Rectangulo(197,207,123),
        D_rojo = new Rectangulo(197,221,123),
        D_azul = new Rectangulo(197,236,123)
    ];

    canvas.fillStyle = "white";
    canvas.font = "" + TamañoLetra + "px Arial"


    //canvas.textAlign="center"
    for (i = 0; i < valoresEnLosRectangulos.length;i++)
    {
        var rectangulo = valoresEnLosRectangulos[i];
        canvas.fillText("" + rectangulo.cantidad, rectangulo.posicionX, rectangulo.posicionY + TamañoLetra);
    }    
}
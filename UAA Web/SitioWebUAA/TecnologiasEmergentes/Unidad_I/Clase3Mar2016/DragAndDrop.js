﻿///
/* Drag and Drop
Eventos
dragstart.- Este evento es dipsaprado en el momento en el que el arrastre comienza.
drag.- Similar a mousemove, pero en una operacionde arrastre
dragend.- Cuando la operacion de arrastrar y soltar finaliza
dragenter.- Cuando el puntero del raton enra dentro del area ocupara por los posibles elementos destino durante una
            operacion de arrastrar y soltar, este evento es disparado.
dragover.- Similar a mousemove, disparado duante una operacion de arrastre por posibles elementos destino
drop.- Cuando el elemento de origen es soltado durante una operacion de arrastrar y soltar, es disparado por el 
        elemento destino.
dragleave.- El raton sale del area ocupada por un elemento por una operacion de arrastrar y soltar. Usado con dragenter para 
            mostrar ayuda visual
*/
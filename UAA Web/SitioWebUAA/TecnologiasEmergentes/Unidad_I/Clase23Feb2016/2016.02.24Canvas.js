﻿/*Canvas*/

///Rectangulos
//Relleno
fillStyle = "rgb(200,0,0)";

/*Ejemplo:*/
var ctx=canvas.getContext ("2d");
ctx.fillStyle="rgb(200,0,0)";

-fillRect(x,y,width,height);  //Con relleno
-strokeRect(x,y,width,height);//Contorno
-clearReact(x,y,width,height);//Transparente

function draw()
{
	var canvas=document.getElementById("tutorial");
	if(canvas.getContext)
	{
		var ctx=canvas.getContext("2d");
		ctx.fillRect(25,25,100,100);
		ctx.clearReact(45,45,60,60);
		ctx.strokeRect(50,50,50,50);
	}
}

///Linea
/*
	BeginPath()
	ClosePath()
	stroke() ... Forma con contorno
	Fill() ...	Forma solida (sustituye clasePath())
	MoveTo(x,y) ... Levantar pixel y colocado en el siguiente punto
	LinTro(x,y)... Linea recta
*/

///Arcos
/*
	arc(x,y,radius,startAngle,endAngle,anticlockwise);
								radianes boolean
*/

///Rectangulo
/*
	rect(x,,width,height)
	Curvas Rezise y ccuadraticas
	-QuadraticCurveTo(ep1x,cp1y,x,y)
	-bezierCurveTo(cp1x,cp1y,cp2x,cp2y,x,y)
*/

///LineTo:
/*
	ctx.beginPath();
	ctx.moveTo(75,50);
	ctx.lineTo(100,75);
	ctx.lineTo(100,25);
	ctx.fill();
--------------------------
	ctx.beginPath();
	ctx.arc(75,75,50,0,Math.Pi*2,true);
	ctx.moveTo(110,75);
	ctx.arc(75,75,35,0,math.Pi,false);
	ctx.moveTo(65,65);
	.
	.
	.
	ctx.stroke();
*/

///Quadratic...
/*
	ctx.beginPAth();
	ctx.moveTo(75,25);
	ctx.quadraticCurveTo(25,25,25,62.5);
	ctx.quadreticCruveTo(25,100,50,100);
	.
	.
	.
------------------------------------------
///Rezier
	ctx.beginPath();
	ctx.moveTo(75,40);
	ctx.bezierCurveTo(75,37,70,25,50,25);
	.
	.
	.
	ctx.fill();
*/
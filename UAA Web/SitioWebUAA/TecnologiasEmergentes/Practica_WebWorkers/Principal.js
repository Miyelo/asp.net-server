﻿var ResultadoSection;
var ButtonIniciar;
var trabajadores=[];
var NumeroActualCalcular = 0;
var HayParaCalcular;
var FechaInicial, FechaFinal;
var CantidadDeWorkeres=3,
    NumeroMaximo=900;

window.addEventListener('load', iniciar, false);
function iniciar()
{
    ResultadoSection = document.getElementById('ResultadoSection');
    ButtonIniciar = document.getElementById('ButtonIniciar');
    ButtonIniciar.addEventListener('click', enviar, false);
}


///Al hacer click en Iniciar
function enviar()
{
    FechaInicial = Date.now();
    HayParaCalcular = true;

    NumeroActualCalcular = 0;
    document.getElementById('ResultadoSection').innerHTML = "";
    document.getElementById('ResultadoIncorrectoSection').innerHTML = "";

    CantidadDeWorkeres =parseInt(document.getElementById('numeroTrabajadores').value);
    NumeroMaximo = parseInt(document.getElementById('numeroMaximo').value);

    for (i = 0; i < CantidadDeWorkeres; i++)
        trabajadores.push(new Worker('CalcularNumerosNarcicistas.js'));

    for (i = 0; i < trabajadores.length; i++)
    {
        trabajadores[i].onmessage = recibir;
        trabajadores[i].postMessage([i, ++NumeroActualCalcular]);        
    }   
}


///Definir Trabajador
function recibir(e)
{
        if (e.data.Coincide == true)
            document.getElementById('ResultadoSection').innerHTML +=
            "<br><div id='CuadroResultadoCorrecto'>Correcto>Numero Calculado: "
            + e.data.NumeroACalcular + " = " + e.data.Resultado +
            "</div>";
        else
            document.getElementById('ResultadoIncorrectoSection').innerHTML +=
            "<br><div id='CuadroResultadoIncorrecto'>Incorrecto>Numero Calculado: "
            + e.data.NumeroACalcular + " = " + e.data.Resultado +
            "</div>";

        //Condicion de Paro
        if (e.data.NumeroACalcular < NumeroMaximo)
        {
            trabajadores[e.data.NumeroWorker].postMessage([e.data.NumeroWorker, ++NumeroActualCalcular]);
            cambiarTiempo()
        }
}

function cambiarTiempo()
{
    FechaFinal = Date.now();
    milisegundosTotales = Math.abs(FechaFinal - FechaInicial);
    document.getElementById('TiempoFinal').innerHTML =
    + parseInt((milisegundosTotales / (1000 * 60 * 60)) % 24) + ":"
    + parseInt((milisegundosTotales / (1000 * 60)) % 60) + ":"
    + parseInt((milisegundosTotales / 1000) % 60)
}
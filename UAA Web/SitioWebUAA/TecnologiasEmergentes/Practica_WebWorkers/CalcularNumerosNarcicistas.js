﻿function ResultadoCalculo(NumeroWorker, NumeroACalcular, Resultado, Coincide) {
    this.NumeroWorker = NumeroWorker;
    this.NumeroACalcular = NumeroACalcular;
    this.Resultado = Resultado;
    this.Coincide = Coincide;    
};

//addEventListener('message', recibido, false);

////function recibido(e)
////{
////    postMessage(calcularNumeroNarcisista(e.data[0],e.data[1]));
////}

function calcularNumeroNarcisista(NumeroWorker,NumeroParaCalcular)
{
    var NumeroCalculado=0,
        TamañoNumero = (NumeroParaCalcular+"").length;

    //Recorremos los numero
    for (i =TamañoNumero;i>0;i--)
    {
        var NumeroActual = (NumeroParaCalcular+"").charAt(i-1);
        NumeroCalculado += Math.pow(NumeroActual, TamañoNumero);
    }
    return new ResultadoCalculo(NumeroWorker,
        NumeroParaCalcular, NumeroCalculado,
        NumeroParaCalcular == NumeroCalculado ? true : false);
}

onmessage=function(e)
{
    postMessage(calcularNumeroNarcisista(e.data[0], e.data[1]));
}

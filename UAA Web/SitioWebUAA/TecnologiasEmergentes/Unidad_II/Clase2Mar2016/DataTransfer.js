﻿/*
    Objeto con informacion en una operacion arrastrar y soltar
    Metodos y propiedades:
       -setData(tipo,dato) datos a ser enviados y su tipo.
    tipos:  -text/plain
            -text/html
            -text/urinList
            -URL
            -Text
        -getData(tipo)
            Retorna datos enviados por el origen, pero solo del tipo especificado

        -clearData() 
            Remueve los datos del tipo especificado.
        -setDragImage(elemento, x ,y )
            Imagen en minuatura junto al puntero del raton que representa al elemento que esta siendo arrastrado.
        -type
            Array con tipos de datos que fueron declarados durante el evento dragStart
        -files.- 
            Array con informacion acerca de los archivos arrastrados.
        -dropEffect.-
            tipo de operacion actualmente seleccionada. 
                (none, copy, link y move)
            effectAllowed.- Tipos de operaciones permitidas.
                (none,copy,copyLink,copyMove,link,
                    linkMove, move, all y unitialized)
*/
function iniciar(){
	cajadatos=document.getElementById('cajadatos');
	var boton= document.getElementById('boton');
	boton.addEventListener('click',enviar,false);
	trabajador=new Worker('trabajador.js');
	trabajador.addEventListener('message',recibido,false);
	trabajador.addEventListener('error',error,false);
}
function enviar(){
	var nombre= document.getElementById('nombre').value;
	if(nombre=='cerrar1'){
		trabajador.terminate();
		cajadatos.innerHTML='trabajador detenido';
	}else{
		trabajador.postMessage(nombre);
	}
}
function recibido(e){
	cajadatos.innerHTML=e.data;
}
function error(e){
	cajadatos.innerHTML = 'ERROR, '+e.message+'<br>Archivo, '+e.filename+'<br>Linea, '+e.lineno;
}
window.addEventListener('load',iniciar,false);
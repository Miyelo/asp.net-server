﻿/*Clases de la tienda*/
var Cancion = function (nombreCancion, IdAlbum, Duracion) {
    this.NombreCancion=nombreCancion,
    this.IdAlbum=IdAlbum,
    this.Duaracion=Duracion
},
    Album = function () {
        NombreAlbum,
        IdGrupoMusical
    },
    Artista = function () {
        NombreArtista,
        IdGrupoMusical,
        FechaNacmiento
    },
    GrupoMusica = function () {
        NombreGrupoMusical,
        FechaCreacion
    }
;


/*Indexed siempre y cuando implemente*/
var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
var dataBase;
/**/
function startDB()
{
    /*Borrar base de datos: Emergencia al cambiar de version*/

    //var req = indexedDB.deleteDatabase('canciones');
    //req.onsuccess = function () {
    //    console.log("Deleted database successfully");
    //};
    //req.onerror = function () {
    //    console.log("Couldn't delete database");
    //};
    //req.onblocked = function () {
    //    console.log("Couldn't delete database due to the operation being blocked");
    //};


    /*Abrir la bd */
    dataBase = indexedDB.open('canciones', 1);

    

    dataBase.onupgradeneeded=function (e)
    {
        var connectionDB = dataBase.result;

        /*Crear Cancion*/
        var cancion= connectionDB.createObjectStore('cancion',{keyPath:'IdCancion',autoIncrement:true});
        cancion.createIndex('by_NombreCancion', 'NombreCancion', { unique: false });
        cancion.createIndex('by_Duracion', 'Duracion', { unique: false });
        cancion.createIndex('by_IdAlbum', 'IdAlbum', { unique: false });

        /*Album*/
        var Album = connectionDB.createObjectStore('album', { keyPath: 'IdAlbum', autoIncrement: true });
        Album.createIndex('by_NombreAlbum', 'NombreAlbum', { unique: false });
        Album.createIndex('by_IdGrupoMusical', 'IdGrupoMusical', { unique: false });

        /*GrupoMusical*/
        var GrupoMusical = connectionDB.createObjectStore('grupo_musical', { keyPath: 'IdGrupoMusical', autoIncrement: true });
        GrupoMusical.createIndex('by_NombreGrupoMusical', 'NombreGrupoMusical', { unique: false });
        GrupoMusical.createIndex('by_FechaCreacion', 'FechaCreacion', { unique: false });

        /*Artista*/
        var Artista = connectionDB.createObjectStore('artista', { keyPath: 'IdArtista', autoIncrement: true });
        Artista.createIndex('by_NombreArtista', 'NombreArtista', { unique: false });
        Artista.createIndex('by_FechaNacimiento', 'FechaNacimiento', { unique: false });
        Artista.createIndex('by_IdGrupoMusical', 'IdGrupoMusical', { unique: false });
    }

    dataBase.onsuccess = function (e) {
        alert('Base de Datos Inicializada');
        cargarCanciones();
    }

    dataBase.onerror = function (e) {
        alert('Error al cargar la Base de Datos');
    }
}

/*Alta de Artistas*/


/*Alta de Canciones*/
function AltaCancion()
{
    var nombreCancion=document.querySelector('#nombreCancion').value,
        idAlbum = document.querySelector('#idAlbum').value,
        duaracion = document.querySelector('#duaracion').value;
    AgregarCancionBD(nombreCancion, idAlbum, duaracion);

    nombreCancion = document.querySelector('#nombreCancion').value = '';
    idAlbum = document.querySelector('#idAlbum').value = '';
    duaracion = document.querySelector('#duaracion').value = '';

}

function AgregarCancionBD (nombreCancion, IdAlbum, Duracion) {
    var activeConnection = dataBase.result;
    var data = activeConnection.transaction(["cancion"], "readwrite");
    var object = data.objectStore("cancion");
    var cancion = new Cancion(nombreCancion, IdAlbum, Duracion)
    var request = object.put(cancion);
    var fueSatisfactorio;

    request.onerror = function (e) {
        alert("Error al de alta cancion");
        fueSatisfactorio = false;
    }
    request.onsuccess = function (e) {
        alert("Alta de " + cancion.NombreCancion + " completada");
        fueSatisfactorio = true;
        cargarCanciones(0);
    }
}

/*carga de Canciones*/
function cargarCanciones()
{
    var active = dataBase.result;
    var data = active.transaction(["cancion"], "readonly");
    var object = data.objectStore("cancion");
                
    var elements = [];
                
    object.openCursor().onsuccess = function (e) {
                    
        var result = e.target.result;
                    
        if (result === null) {
            return;
        }
                    
        elements.push(result.value);
        result.continue();
                    
    };
                
    data.oncomplete = function() {
                    
        var outerHTML = '';
                    
        for (var key in elements) {
                        
            outerHTML += '\n\
                        <tr>\n\
                            <td>' + elements[key].NombreCancion + '</td>\n\
                            <td>' + elements[key].IdAlbum + '</td>\n\
                            <td>' + elements[key].Duaracion + '</td>\n\
                        </tr>';                        
        }
                    
        elements = [];
        document.querySelector("#listaCanciones").innerHTML = outerHTML;
    };
}


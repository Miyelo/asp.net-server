﻿
var map;
var PuntoInicial = new google.maps.LatLng(21.914548, -102.316991);
var CostoPorDistancia = 2.25,
    CostoBanderaso=10.50;

function initialize() {
    var mapProp = {
        center: PuntoInicial,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
    placeMarkerWithoutInfoWindow(PuntoInicial);

    var myCity = new google.maps.Circle({
        center: new google.maps.LatLng(21.883888, -102.295115),
        radius: 20000,
        strokeColor: "#0000FF",
        strokeOpacity: 0.5,
        strokeWeight: 2,
        fillColor: "#000055",
        fillOpacity: 0.4,
        clickable:false,
    });
    myCity.setMap(map);

    google.maps.event.addListener(map, 'click', function (event)
    {
        placeMarkerWithInfoWindow(event.latLng);
    });    
}

function placeMarkerWithInfoWindow(location) {
    
    var X1 = PuntoInicial.lat();
    var Y1 = PuntoInicial.lng();
    var X2 = location.lat();
    var Y2 = location.lng();

    
    var distancia =Math.sqrt(Math.pow((X2 - X1),2) + Math.pow((Y2 - Y1),2))*100;

    var marker = new google.maps.Marker({
        position: location,
        map: map,
    });

    var costo = CostoBanderaso+distancia * CostoPorDistancia;
    if (costo <0)
        costo = costo* -1
    
    var infowindow = new google.maps.InfoWindow({        
        content: 'Costo =$' + costo.toFixed(2)
        + "<br/>"
        +"<a href=SeleccionarTaxi.html >Pedir</a>"
        ,
    });
    infowindow.open(map, marker);
}

function placeMarkerWithoutInfoWindow(location) {
    var marker = new google.maps.Marker({
        position: location,
        map: map,
    });
    var infowindow = new google.maps.InfoWindow({
        content: 'Usted esta Aqui'
    });
    infowindow.open(map, marker);
}
google.maps.event.addDomListener(window, 'load', initialize);
﻿(function ()
{

    var app =angular.module('store', [ ]);
    app.controller('StoreController', function ()
    {
        this.products=gems;
    });

    var gems =
        [
            {
                name: 'Dodecahedron',
                price: 2.95,
                description: 'Text in Description Dodecahedron',
                images:[
                    {
                        full:'../images/dodecahedron-01-full.jpg',
                        thumb: '../images/dodecahedron-01-thumb.jpg'
                    }
                ],
                canPurchase: false,
                soldOut: true,
                reviews: [
                {
                    stars: 5,
                    body: "I love this product!",
                    author:"joe@thomas.com"
                }
                ]
            },
            {
                name: "Pentagonal Gem",
                price: 5.95,
                description: 'Text in Description Pentagonal Gem',
                canPurchase: false,
            }
        ];

    app.controller("PanelController", function () {
        this.tab = 1;
        this.selectTab = function (setTab) {
            this.tab = setTab;
        };
        this.isSelected = function (checkTab) {
            return this.tab == checkTab;
            this.review = {};
        };
    });

    app.controller("ReviewController",function(){
        this.review = {};

        this.addReview = function (product) {
            product.reviews.push(this.review);
            this.review = {};
        };
    });

    app.directive('productTitle', function () {
        return {
            restrict: 'E', //Type of Directive (E for Element)
            templateUrl:'product-title.html' //Url of a Template
        };
    });
})();
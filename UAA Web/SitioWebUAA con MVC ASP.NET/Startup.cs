﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SitioWebUAA_con_MVC_ASP.NET.Startup))]
namespace SitioWebUAA_con_MVC_ASP.NET
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
